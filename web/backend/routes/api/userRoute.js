const express = require('express');
const router = express.Router();

const userCtrl = require('../../app/controllers/userController');
const checkIdentity = require('../../app/middleware/authentication').checkIdentity;

// Without auth
router.post('/signup', checkIdentity, userCtrl.signup);
router.post('/login', checkIdentity, userCtrl.login);

module.exports = router;
