const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const _schema = mongoose.Schema({
    email: {type: String, required: true/*, unique: true*/},
    password: {type: String, required: true},
});

// _schema.plugin(uniqueValidator);
exports.schema = _schema;
exports.model = mongoose.model('User', _schema);
