const mongoose = require('mongoose');

const _schema  = mongoose.Schema({
    lat: {type: Number, default: 0},
    long: {type: Number, default: 0},
});

exports.schema = _schema;
exports.model = mongoose.model('Coordinate', _schema)
