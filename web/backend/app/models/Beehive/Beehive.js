const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const BeehiveArraySchema = require('./BeehiveArray').schema;

const _schema = mongoose.Schema({
    /*ownerId: {type: String, require: true },
    weatherStationId: {type: String, require: true },*/

    guid: {type: String, require: true /*, unique: true*/},
    name: {type: String, default: 'new beehive'},
    isAuthenticated: {type: Boolean, default: false},
    measures: [BeehiveArraySchema],

    lastConnexion: {type: Date, default: Date.now},
    lastHumidity: {type: Number, default: undefined},
    lastTemperature: {type: Number, default: undefined},
});


// _schema.plugin(uniqueValidator);
exports.schema = _schema;
exports.model = mongoose.model('Beehive', _schema);
