const mongoose = require('mongoose');

const _schema = mongoose.Schema({
    // isRemoteServer: {type: Boolean, default: true},
    isConfigured: {type: Boolean, default: false},
    hasGPSModule: {type: Boolean, default: false},
});

exports.schema = _schema;
exports.model = mongoose.model('Configuration', _schema);
