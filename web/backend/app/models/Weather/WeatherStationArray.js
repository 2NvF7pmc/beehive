const  mongoose = require('mongoose');
const measureSchema = require('./WeatherStationMeasure').schema;

const _schema = mongoose.Schema({
    timestamp: {type: Date, default: Date.now},
    data: {type: measureSchema, default: measureSchema},
});

exports.schema = _schema;
exports.model = mongoose.model('WeatherStationArray', _schema);
