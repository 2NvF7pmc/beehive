import React from "react";
import PropTypes from 'prop-types';

class InputType {
    static TEXT = 'text';
    static PASSWORD = 'password';
    static EMAIL = 'email';
    static TEXT_AREA = 'textarea';
    static NUMBER = 'number';
}

/**
 * Generate a input
 * @param id
 * @param type
 * @param value
 * @param children
 * @param onChange
 * @param required
 * @returns {*}
 * @constructor
 */
function Input({id, type, value, children, onChange, required = false}) {

    const _handleChange = e => {
        onChange(e.target.value)
    }

    const _render = () => {
        switch (type) {
            case InputType.TEXT_AREA:
                return (<div className="mb-3 row">
                    <label htmlFor={id} className="col-sm-3 col-form-label">{children}</label>
                    <div className="col-sm-9">
                        <textarea id={id} className="form-control" onChange={_handleChange}>{value}</textarea>
                    </div>
                </div>);

            default:
                if(children){
                    return (<div className="mb-3 row">
                        <label htmlFor={id} className="col-sm-3 col-form-label">{children}</label>
                        <div className="col-sm-9">
                            <input type={type} className="form-control" id={id} onChange={_handleChange}
                                   value={value}/>
                        </div>
                    </div>);
                }else {
                    return (<input type={type} className="form-control" id={id} onChange={_handleChange}
                                   value={value}/>);
                }

        }
    }

    return _render();
}

Input.propTypes = {
    id: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    type: PropTypes.string,
    value: PropTypes.string,
    children: PropTypes.string,
    required: PropTypes.bool
}

export default Input;
export {InputType};