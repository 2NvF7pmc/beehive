import PropTypes from "prop-types";

function DashboardNavbar({wsData: {name, lastConnexion}}) {
    return (<nav className="navbar navbar-expand-lg navbar-light bg-warning">
        <div className="container-fluid">

            <h1 className="navbar-brand">
                {name}-
                <span
                    className="badge bg-secondary">Last update, {new Date(lastConnexion || Date.now()).toDateString()}</span>
            </h1>
            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>

            <div className="collapse navbar-collapse" id="navbarNav">
                <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                    <li className="nav-item"><a className="nav-link" href="#">Edit weather station</a></li>
                </ul>
                <div className="d-flex">
                    <button className="btn btn-outline-danger">Sign Out</button>
                </div>
            </div>

        </div>
    </nav>);
}

DashboardNavbar.propTypes = {
    wsData: PropTypes.object.isRequired
}

export default DashboardNavbar;