# Overview

![alt text](https://gitlab.com/ANODIN/beehive/-/raw/master/doc/weather_station_architecture.png)

# What inside folder

- ./esp8266, contains arduino code for ESP8266
- ./utils, contains code for test, or inject data
- ./web
    - ./backend, contains an API server make with **Express.js**
    - ./frontend, contains **React** web app
