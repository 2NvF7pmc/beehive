/**
   PostHTTPClient.ino

    Created on: 21.11.2016

*/

#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <ArduinoJson.h>
/* this can be run with an emulated server on host:
        cd esp8266-core-root-dir
        cd tests/host
        make ../../libraries/ESP8266WebServer/examples/PostServer/PostServer
        bin/PostServer/PostServer
   then put your PC's IP address in SERVER_IP below, port 9080 (instead of default 80):
*/

//#define SERVER_IP "10.0.1.7:9080" // PC address with emulation on host

#define GUID "715b04eb-5a7e-4ff0-98d9-9e4a9da893d7"
#define SERVER_IP "10.3.141.1:4000"

#ifndef STASSID
#define STASSID "raspi-webgui"
#define STAPSK "ChangeMe"
#endif

#define CONNECT_API "beehive"
#define SEND_MEASURES_API "beehive/client/measures"

bool isAuthenticated = false;

void printHTTP(String msg)
{
  Serial.println("[HTTP] " + msg);
}

String getApiURL(String path)
{
  return "http://" SERVER_IP "/api/" + path;
}

void setup()
{

  Serial.begin(115200);

  Serial.println();
  Serial.println();
  Serial.println();

  WiFi.begin(STASSID, STAPSK);

  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected! IP address: ");
  Serial.println(WiFi.localIP());
}

HTTPClient http;
WiFiClient client;

const size_t capacity = JSON_OBJECT_SIZE(2) + 30;

/**
 * Send data to the server
 * if return true data is correctly added
*//*
bool sendMeasures (int temperature, int humidity){

  String url = getApiURL(SEND_MEASURES_API "/?guid=" GUID "&t=" + temperature + "&h=" + humidity);

  printHTTP('Start send data, url: ' + url);

  if (http.begin(client, url)){
    int httpCode = http.PUT();
    printHTTP(httpCode);
  }

  return false;
}*/

void requestAuthentication()
{
  printHTTP("connection request");

  if (http.begin(client, getApiURL(CONNECT_API "/?guid=" GUID)))
  {
    int httpCode = http.GET();
    if (httpCode > 0)
    {
      DynamicJsonDocument doc(capacity);

      switch (httpCode)
      {
      case HTTP_CODE_ACCEPTED:
      case HTTP_CODE_OK:
        printHTTP(http.getString());
        deserializeJson(doc, http.getString());

        isAuthenticated = doc["isAuthenticated"];
        if (isAuthenticated)
        {
          Serial.println("isAuthenticated");
        }
        break;

      default:
        Serial.println("other code" + httpCode);
        break;
      }
    }
    else
    {
      Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
    }
  }

  http.end();
}

void loop()
{
  // wait for WiFi connection
  if ((WiFi.status() == WL_CONNECTED))
  {
    if (!isAuthenticated)
    {
      // wait for authenticated
      requestAuthentication();
    }
    else
    {
      // wen beehive is connected to the server
      printHTTP("you are auth");
    }
  }

  delay(10000);
}
